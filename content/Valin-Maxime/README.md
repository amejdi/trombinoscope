---
prenom: Maxime
nom: Valin
---
# Profil

![Image de profil](https://cdn.discordapp.com/attachments/1070774583126409300/1154431451664764938/Default_pfp.png)
Etudiant en BTS SIO dans le lycée La Colinière à Nantes. Je suis passionné d'informatique et réalise sur mon temps libre des programmes depuis plusieurs années.

## Contact

- Téléphone : 07 68 81 24 66
- E-mail : <maximevalinpro@gmail.com>
- Réseaux Professionnels : [LinkedIn](https://www.linkedin.com/in/maxime-valin-05a816292/)


# Formations

- [2017-2020] **Collège Agnes Varda**, _Ligné_
- [2020-2023] **Lycée Caroline Aigle**, _Nort-sur-Erdre_
- [2023-????] **BTS SIO La Colinière**, _Nantes_


# Compétences

| Compétence | Caractéristiques |
|-------------------------- | -----------------------------|
| Langages de Programmation | Python, Java, JavaScript, C# |
| Systèmes d'Exploitation | Windows, Ubuntu, Debian |
| Langues | Français, Anglais |


# Experiences Professionnelles

- [2019] **Stage d'observation** dans le cadre de la classe de troisième dans l'entreprise _Mercuria_.
