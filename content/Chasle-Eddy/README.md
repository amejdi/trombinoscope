---
prenom: Eddy
nom: Chasle
---
# **Profil**

![](https://cdn.discordapp.com/attachments/767361118741987369/1154422078943002673/Capture.PNG)

>Etudiant au sein de l'établissement au Lycée de la Colinière en BTS SIO. Passionné par l'informatique depuis 4 ans, je réalise des projets en tout genre. 

## Contact

 - **Numéro de téléphone** : [06 51 65 68 00](+330651656800)

 - **Adresse mail** : [eddy.chasle@gmail.com](eddy.chasle@gmail.com)

 - **Réseau social** : [LinkedIn](www.linkedin.com/in/eddy-chasle-1338a0265)

# Formation

- **Bac Général Spécialité Mathématiques & NSI** / _Lycée Alcide D'Orbigny - Bouaye_ / 2019 - 2022
- **Expert en Technologies de l’Information** / _Epitech - Nantes_ / 2022 - 2023
- **BTS - Service de l'Information aux Organisations** / _Lycée de la Colinière - Nantes_ / 2023 - En cours

# Compétences

| Compétence | Caractéristiques |
|- | :-|
| Langages de Programmation | Python, Javascript, C# |
| Systèmes d'Exploitation | Windows, Ubuntu, Debian, Fedora |
| Langues | Français, Anglais |
| Divers | Autonomie, Gestion de projet, Travail d'équipe

# Expériences

- **Employé Commercial** / _Hyper U - La Montagne_ / Juillet 2023 -  Août 2023
- **agent d'entretien des espaces verts** / _La Montagne_ / Octobre 2021

## Stage

- **Stage d'oservation 3eme** / _bouygues&Energie - Saint-Herblain_ / Décembre 2019

## Bénévolat

- **Resposable de salle** / _Web2Day - Nantes_ / Juin 2023
- **Employé Service client** / _DevFest - Nantes_ / Octobre 2022
