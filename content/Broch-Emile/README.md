---
Prenom: Emile
Nom: Broch
---
# Profil
![photo de profil provisoire](https://urlz.fr/nHol)
>*Je suis actuellement etudiant en BTS SIO au Collège-Lycée La Collinière à Nantes, 
>mes contact, ainsi que beaucoup d'autre information me concernant son disponible ici.*
>> ## Informations :
>>
>> - Mail professionnel: <Emile.Broch.mail@gmail.com>
>> - Numero de telephone : [07 87 11 32 45](0787113245)
>> - A venir ...

# Compétences
| Compétences | Détails |
|-------------|---------|
| Langage de programation | Html5, CSS3, PHP, SQL, Javascript, Java |
| Langue | Français, Anglais, Espagnol |
| Logiciels maitrisés | Affinity(Photo;Designer;Publisher), Jmerise, VsCode, PremierPro |
# Formations 
>
> - {2020}-{2022} **BAC STI2D SIN** *Lycée Jean-Perrin*
> - {2022}-{2023} **BUT MMI** *IUT Laval*
> - {2023}-{20XX} **BTS SIO (SLAM/SISR)** *Collège-Lycée La Collinière*
## Formations Professionnelles
>| Stages | Durée | Annee |
>|--------|-------|-------|
>|Nantes métropoles Service informatique | 1 semaine | 2019 |
>| Ludwig informatique | 2 semaines | 2020 |
