---
prenom: Gabriel
nom: Effoudou-Ollivier
---
# Profil :
![enter image description here](https://mail.google.com/mail/u/0?ui=2&ik=ca4904a128&attid=0.1&permmsgid=msg-a:r-1067770291209820143&th=18adbfbf1723e81d&view=att&disp=safe&realattid=f_ln37q3k80)  
Je m'appelle Gabriel Effoudou-Ollivier, j'ai 19ans et je suis étudiant en 1ère année de BTS SIO à Nantes. 
## Contacts :
Téléphone : [0633032937](tel:+33633032937)  
e-mail : [gabrieleffoll@gmail.com](mailto:gabrieleffoll@gmail.com)                        
# Formation :
- **BTS :** - SIO 1ère année (Nantes,2023-2024)  
- **Licence Biologie** - accès santé (Rennes, 2022-2023)    
- **Terminale générale**, spécialités : mathématiques, SVT (Rennes, 2021-2022)  
- **Première générale**, spécialités : mathématiques, physique/chimie et SVT (Rennes,2020-2021)     
- **Seconde générale** , (Rennes, 2019-2020)      
# Experience pro : 
-Stage d'observation chez un opticien (2019)  
-Stage d'observation laboratoire d'astrologie à Nantes (2019)  
-Jobs étudiants : babysitting, agent d'entretien   
# compétences
|compétences| connaissances |
|--|--|
|**Programmation**| python, markdown |
|**Systèmes d'exploitation**| Windows, Linux |
|**Langues vivantes**| Français, Anglais, Allemand  



