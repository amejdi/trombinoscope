---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Alfeze
nom: Ali

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil
Je suis Ali Alfeze, un jeune de 18 ans résidant à Nantes. J'ai obtenu mon baccalauréat STI2D option Systèmes d'Information et Numérique (SIN) avec mention, et actuellement, je poursuis mes études en informatique en première année de BTS SIO au lycée La Colinière, situé également à Nantes.

En tant qu'étudiant en SIO, je partage avec mes camarades une passion commune pour l'informatique et les avancées technologiques qui façonnent notre quotidien. Ma curiosité naturelle et mon engagement sérieux me poussent à envisager une carrière future dans le domaine de l'informatique, que ce soit en tant que développeur web ou en cybersécurité, en tant que pentester. Mon objectif est de contribuer à l'évolution et à la sécurité du monde numérique.

## Contact
Numéro de téléphone :
Email : aali.informatique@gmail.com

