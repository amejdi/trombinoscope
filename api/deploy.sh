#
# Script de déploiement
# Le script s'execute automatiquement par CI/CD
#

# fonctions du script
source api/function.sh

# on créer un fichier temporaire pour la page index du site
INDEX=$(mktemp /tmp/foo-XXXXX)

# on parcourt les éléments contenus dans content
for content in content/*; do

    # on initialise les variables de metadonnées
    unset prenom nom avatar_url avatar_img style

    # on récupère le nom du dossier de l'élève
    student=$content
    student_dir=$(echo $student/ | cut -d '/' -f 2 )

    # on contrôle que le nom du dossier lu est conforme au spécification (Nom-Prénom)
    if [ -d "$content" ] && [[ $( [ $(echo "$student_dir" | grep -E "[A-Za-z]+-[A-Za-z]+") ] && echo true ) ]]; then

        echo "Traitement du dossier \`$student\`
        "
        echo "  📂 $student"

        # on recherche le fichier README.md dans le dossier
        content_filepath=$(find $student -iname "*.md" -print)

        # on contrôle que le fichier trouvé est bien un fichier
        if [ -f $content_filepath ] ; then

            echo "   ↳ 📄 $content_filepath"

            # on génère le chemin du fichier html cible
            make_url=$( echo $student_dir | tr '[:upper:]' '[:lower:]' | sed 's/áàâäçéèêëîïìôöóùúüñÂÀÄÇÉÈÊËÎÏÔÖÙÜÑ/aaaaceeeeiiiooouuunAAACEEEEIIOOUUN/g' | sed 's/ /-/g' )
            public_filepath="public/$make_url.html"

            # on lit les variables du fichier README
            eval $( parse_yaml $content_filepath )

            # on controle que les variable de metadonnées sont bien alimentées
            if [[ -n $prenom ]] && [[ -n $nom ]]; then 

                echo "      ↳ 🏷️  prenom = $prenom"
                echo "      ↳ 🏷️  nom    = $nom"

                # on recherche l'image de profil dans le fichier README 
                avatar_alt=$( cat $content_filepath | grep -m 1 -oE '!.+\(.+\)' | grep -oE '\[.+\]' | tr '[]' '  ' | xargs )
                avatar_url=$( cat $content_filepath | grep -m 1 -oE '!.+\(.+\)' | grep -oE '\(.+\)' | tr '()' '  ' | xargs )

                if [[ -n $avatar_url ]]; then
                    avatar_img="<img src=\"$avatar_url\" alt="$avatar_alt">"
                    echo "      ↳ 🖼️  avatar = $avatar_url"
                else
                    avatar_img="<img src=\"media/default.jpg\" alt=\"Image de profil par défaut représentant un personnage stylisé\" loading=\"lazy\">"
                fi

                # on ajoute le nom et prénom de l'élève dans le fichier index
                echo "- <a href=\"./$make_url.html\" title=\"Accéder à la fiche de $prenom $nom\"><div>$avatar_img</div> <h3>$prenom $nom</h3></a>" >> $INDEX

                # on controle s'il existe une feuille de style personnalisée
                if [ -f "$student/asset/style.css" ]; then
                    style=$(cat $student/asset/style.css)
                fi

                # on convertit le fichier Markdown en HTML via pandoc
                pandoc \
                    -s \
                    -f markdown-auto_identifiers -t html5 \
                    --template template/student.html \
                    --metadata title="$prenom $nom" \
                    --metadata style="$style" \
                    --shift-heading-level-by=1 \
                    -c style.min.css \
                    $content_filepath \
                    -o $public_filepath

                echo ""
                echo "✅ Fichier HTML généré
                "
            else
                echo "Les variables \`prenom\` et \`nom\` ne sont pas trouvées dans le fichier README"
            fi
            

        else
            echo "❌ \`$content\` non comforme au format attendu
            "
        fi
    else
        echo "❌ \`$content\` n'est pas un dossier ou son nom n'est pas conforme au format attendu
        "
    fi

    echo "―――
    "
    
done

# On convertit le fichier temporaire du format Markdown au format HTML
pandoc \
        -s \
        -f markdown -t html5 \
        --template template/index.html \
        --metadata title="Index" \
        -c style.min.css \
        $INDEX \
        -o "public/index.html"

# On supprime le fichier temporaire
rm $INDEX
    